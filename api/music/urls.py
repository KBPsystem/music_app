from django.urls import path
from .views import ListCreateSongsView, SongsDetailView, LoginView, RegisterUsers, ListSongsView

urlpatterns = [
    path('songs/', ListCreateSongsView.as_view(), name="songs-list-create"),
    path('songs/<int:pk>/', SongsDetailView.as_view(), name="songs-detail"),
    path('auth/register/', RegisterUsers.as_view(), name="auth-register"),
    path('auth/login', LoginView.as_view(), name="auth-login")
]