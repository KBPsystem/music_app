from django.db import models

# Create your models here.
class Songs(models.Model):
    # Song title
    title = models.CharField(max_length=255, null=False)
    # Name of Artist
    artist = models.CharField(max_length=255, null=False)

    def __str__(self):
        return "{} - {}".format(self.title, self.artist)
