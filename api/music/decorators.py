from rest_framework.response import Response
from rest_framework.views import status

def validate_request_data(fn):
    def decorated(*args, **kwargs):
        title = args[0].request.data.get("title", "")
        artist = args[0].request.data.get("artist", "")
        if not title and artist:
            return Response(
                data={
                    "message": "Both Title and Artist are required to add a song"
                },
                status=status.HTTP_400_BAD_REQUEST
            )
            return fn(*args, **kwargs)
        return decorated