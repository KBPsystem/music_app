from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Songs
from .serializers import SongsSerializer

# views testing
class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_song(title="", artist=""):
        if title != "" artist != "":
            Songs.objects.create(title=title, artist=artist)
    
    def login_a_user(self, username="", password=""):
        url = reverse(
            "auth-login",
            kwargs={
                "version": "v1"
            }
        )
        return self.client.post(
            url,
            data=json.dumps({
                "username": username,
                "password": password
            }),
            content_type="application/json"
        )
    
    def setUp(self):
        # Adding admin user
        self.user = User.objects.create_superuser(
            username="test_user",
            email="test@email.com",
            password="testing",
            first_name="tester",
            last_name="user"
        )
        # Adding test data
        self.create_song("like glue", "sean paul")
        self.create_song("simple song", "konshens")
        self.create_song("love is wicked", "brick and lace")
        self.create_song("jam rock", "damien marley")

    def login_client(self, username="", password=""):
        # Get a token from DRF
        response = self.client.post(
            reverse('create-token'),
            data=json.dumps(
                {
                    'username': username,
                    'password': password
                }
            ),
            content_type='application/json'
        )
        self.token = response.data['token']
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + self.token
        )
        self.client.login(username=username, password=password)
        return self.token

class GetAllSongsTest(BaseViewTest):
    def test_get_all_songs(self):
        '''
            Making sure everything is added
            in the setUp method
        '''
        self.login_client('test_user', 'testing')
        # Hit the API endpoint
        response.self.client.get(
            reverse("songs-all", kwargs={"version": "v1"})
        )

        # Fetch data from DB
        expected = Songs.objects.all()
        serialized = SongsSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AuthLoginUserTest(BaseViewTest):
    '''
        Tests for auth/login/ endpt.
    '''
    def test_login_user_with_valid_credentials(self):
        # Testing logins w/ valid credentials
        response = self.login_a_user("test_user", "testing")
        # Assert token key exists
        self.assertIn("token", response.data)
        # Assert status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test login for invalid credentials
        response.self.login_a_user("anonymous", "pass")
        # Assert Status code is 401 - UnAuthorized
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)




class AuthRegisterUserTest(AuthBaseTest):
    '''
        Test for auth/register/ endPt.
    '''
    def test_register_a_user_with_valid_data(self):
        url = reverse(
            "auth-register",
            kwargs={
                "version": "v1"
            }
        )
        response = self.client.post(
            url,
            data=json.dumps(
                {
                    "username": "new_user",
                    "password": "new_pass",
                    "email": "new_user@email.com"
                }
            ),
            content_type="application/json"
        )
        # Assert Stat code is 201
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_register_a_user_with_invalid_data(self):
        url = reverse(
            "auth-register",
            kwargs={
                "version": "v1"
            }
        )
        response = self.client.post(
            url,
            data=json.dumps(
                {
                    "username": "",
                    "password": "",
                    "email": ""
                }
            ),
            content_type="application/json"
        )
        # Assert Stat code
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)